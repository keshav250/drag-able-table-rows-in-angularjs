import { Component, OnInit } from '@angular/core';
import {CdkDragDrop,moveItemInArray} from '/drag-table/@angular/cdk/drag-drop';

@Component({
  selector: 'app-dragable-table',
  templateUrl: './dragable-table.component.html',
  styleUrls: ['./dragable-table.component.scss']
})
export class DragableTableComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'title',
    'is_new',
    'status',
  ];

  statusTypes = [
    { value: 'PUBLISHED', label: 'Published' },
    { value: 'DRAFT', label: 'Draft' },
  ];
  previousSequence: any = null;
  currentSequence: any = null;
  topics: any = [];


  constructor( 
    // private apiService: ApiService,
    DragDropModule,
    ) { }

  ngOnInit(): void {
    this.topics = topicsData;
  }

  drop(event: CdkDragDrop<string[]>) {

    // commented code can be used if u got an api for the data order to be saved

    if (!this.previousSequence) {
      this.previousSequence = topicsData.map((topic) => topic.id);
      // this.previousSequence = this.topics.map((topic) => topic.id);
    }
    moveItemInArray(topicsData, event.previousIndex, event.currentIndex);
    this.currentSequence = topicsData.map((topic) => topic.id);
    // moveItemInArray(this.topics, event.previousIndex, event.currentIndex);  
    // this.currentSequence = this.topics.map((topic) => topic.id);      

    // this.apiService.updateResourceTopicOrder({
    //   topics_order: this.currentSequence
    // }).subscribe(
    //     (res: any) => {
    //       this.topics = res
    //       this.loading = false;
    //       // this.getCategories()
    //     },
    //     (err) => {
    //       this.loading = false;
    //       console.log(err);
    //     }
    //   );
  }

}


var topicsData = [
  {
    id: '1',
    name: 'Record 1',
    status: 'PUBLISHED',
    is_new: false,
  },
  {
    id: '2',
    name: 'Tax Credit & Filing',
    status: 'Record 2',
    is_new: false,
  },
  {
    id: '3',
    name: 'Record 3',
    status: 'PUBLISHED',
    is_new: false,
  },
  {
    id: '4',
    name: 'Record 4',
    status: 'PUBLISHED',
    is_new: false,
  },
  {
    id: '5',
    name: 'Record 5',
    status: 'PUBLISHED',
    is_new: false,
  },
  {
    id: '6',
    name: 'Record 6',
    status: 'DRAFT',
    is_new: false,
  },
  {
    id: '7',
    name: 'Record 7',
    status: 'DRAFT',
    is_new: true,
  },
  {
    id: '8',
    name: 'Record 8',
    status: 'PUBLISHED',
    is_new: true,
  },
];
